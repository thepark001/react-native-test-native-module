package com.testnativemodule;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;

public class LetterboxPlayerManager extends SimpleViewManager<View> {
    @Override
    public String getName() {
        return "RCTLetterboxPlayer";
    }

    ViewGroup LetterboxPlayer;

    @Override
    public View createViewInstance(ThemedReactContext context) {
        LetterboxPlayer = new RelativeLayout(context);
        LetterboxPlayer.setBackgroundColor(Color.RED);

        return LetterboxPlayer;
    }
}
