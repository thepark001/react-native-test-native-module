import { NativeModules } from 'react-native';

const { LetterboxAndroid } = NativeModules;

export default LetterboxAndroid;
