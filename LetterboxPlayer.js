import React from 'react';
import {requireNativeComponent} from 'react-native';

const LetterboxPlayer = props => <RCTLetterboxPlayer style={{flex: 1}} />;

const RCTLetterboxPlayer = requireNativeComponent(
  'RCTLetterboxPlayer',
  LetterboxPlayer,
);

export default LetterboxPlayer;
